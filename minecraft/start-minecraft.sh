#!/bin/bash
if ! tmux has-session -t minecraft 2>/dev/null; then
    /usr/bin/tmux new-session -s minecraft -d
    tmux send -t minecraft "/usr/local/games/minecraft/bedrock_server" ENTER
else
    echo "minecraft session already exists"
fi
