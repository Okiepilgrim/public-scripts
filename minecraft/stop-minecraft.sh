#!/bin/bash
/usr/bin/tmux send -t minecraft "stop" ENTER
sleep 2
echo "Stopping Minecraft tmux session"
/usr/bin/tmux kill-session -t minecraft
