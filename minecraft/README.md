## Minecraft Scripts
Scripts for running your own Minecraft Bedrock Edition Server on a linux host. (Created on Ubuntu 20.04)

## Prerequisites
These scripts assume
- there is a *minecraft* user with sufficient homedir space to download the bedrock server zip files
- Minecraft has already been manually installed in /usr/local/games/<minecraft-bedrock-server.x.x.x>
- there is a symbolic link to the game folder at /usr/local/games/minecraft

NOTE: The *minecraft* user is also a member of the root group and the /usr/local dir permissions were updated with group write (chmod 0775)

## Setup
- Drop the minecraft.service file into /etc/systemd/system
- Set the permissions/ownership of that file to match other services e.g. `sudo chmod 0644 /etc/systemd/system/minecraft.service`
- Drop the stop and start scripts into /usr/local/bin/
- Set the permissions/ownership of those files 
- e.g. `sudo chown minecraft:root start-minecraft.sh` and `sudo chmod 0755 /usr/local/bin/start-minecraft.sh`

## Notes
These scripts launch the minecraft server in a tmux session so that you can attach and detach to the server console to run commands or check on the server status.
My kids like to play both creative and survival, so I run one server of each, so you will see "duplicate" code for this scenario sprinkled throughout.
There is also a "cron" script that calls the update script and restarts the services. I have this set to run daily, but do what works for you.
