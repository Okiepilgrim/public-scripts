#!/bin/bash
echo "#### Running update script on: " >> /usr/local/games/minecraft-updates.log
date >> /usr/local/games/minecraft-updates.log
if [ `whoami` != "minecraft" ]; then
  echo "Not minecraft user!" | tee -a /usr/local/games/minecraft-updates.log
  exit -1
fi

# Switch to a temp dir in the minecraft user home dir
cd ~/temp/

# Remove anything in temp
rm -rf ~/temp/*

# Get the latest webpage of the Minecraft Download Site containing the current version
curl --ssl -A 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/97.0.4692.71 Safari/537.36' -o bedrock.html https://www.minecraft.net/en-us/download/server/bedrock
# Now Download the current version
wget `grep -o 'https.*/bin-linux/.*\.zip' bedrock.html`
# save the current version to version.txt
ls -l | grep -o 'bedrock-server.*\.zip' | sed 's/.zip//' > version.txt

# Set some vars
Zipfile=`ls bedrock-server*`
Version=`cat version.txt`
#echo "$Version"

# Check current version
Current=`ls -l /usr/local/games/minecraft | grep -o 'bedrock-server-.*/' | sed 's/\///'`
echo "Current Version: $Current" >> /usr/local/games/minecraft-updates.log
echo "New Version: $Version" >> /usr/local/games/minecraft-updates.log
if [ $Current = $Version ]; then
  echo "Already up to date. Exiting" | tee -a /usr/local/games/minecraft-updates.log
  exit 0
fi

echo "filename: $Zipfile" >> /usr/local/games/minecraft-updates.log

### Note minecraft user is in root group and /usr/local dir has group write permissions
# Make a new directory for the latest version and unzip the latest version in that folder
mkdir /usr/local/games/minecraft-`echo "$Version"`
cd /usr/local/games/minecraft-`echo "$Version"`
unzip /home/minecraft/temp/`echo "$Zipfile"`
# backup default properties (in case new options/settings exist)
mv server.properties server.properties.orig
# Copy the necessary settings from the previous version folder (currently symlinked with generic "minecraft")
cp ../minecraft/server.properties .
cp ../minecraft/permissions.json .
cp ../minecraft/allowlist.json .
# Copy over all of the game data -- NOTE: THIS DUPLICATES GAME DATA - essentially creating "snapshots" or versions
# This script DOES NOT do any cleanup of historical data or older server versions.
cp -r ../minecraft/worlds/ .
# Change to the "root" of where minecraft lives
cd /usr/local/games
# Remove the old symlink
rm minecraft
# Create a new symlink
ln -s minecraft-`echo "$Version"`/ minecraft

### Below here is only necessary if you are running multiple instances and have everything duplicated appropriately

## Repeat for Survival Server (aka minecraft2)
mkdir /usr/local/games/minecraft-`echo "$Version"`-survival
cd /usr/local/games/minecraft-`echo "$Version"`-survival
unzip /home/minecraft/temp/`echo "$Zipfile"`
mv server.properties server.properties.orig
cp ../minecraft2/server.properties .
cp ../minecraft2/permissions.json .
cp ../minecraft2/allowlist.json .
cp -r ../minecraft2/worlds/ .
cd /usr/local/games
rm minecraft2
ln -s minecraft-`echo "$Version"`-survival/ minecraft2
