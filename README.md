# public-scripts

Misc scripts, config files, etc.

## Description
A very small collection of scripts used to automate various tasks in my home network.

## Support
Scripts are provided as-is, with no warranty of any kind for any purpose. Feel free to open an issue (or submit a merge request) if you find something broken or needing improvement.

## Contributing
If you're interested in contributing to anything or submitting any fixes/improvements, feel free to submit a merge request.

## License
The scripts here are free and unencumbered software released into the public domain. See "Unlicense" licence file for additional details.
